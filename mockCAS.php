<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Authentication Plugin: CAS Authentication
 *
 * Authentication using CAS (Central Authentication Server).
 *
 * @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package auth_eleacas
 */


/**
 * Class used to mock phpCAS in phpunit tests
 */

class mockphpCAS {
	
	static function getUser() {
		return "tstark";
	}

	static function getAttributes() {
		return array(
			'prenom' => 'Tony',
			'nom' => 'Stark',
			'mail' => 'tony.stark@ac-versailles.fr',
			'description' => 'I am Iron Man',
			'FrEduRne' => 'shield$xx$yy$zz$shield'
		);
	}
	
	static function client($protocol, $hostname, $port, $baseuri, $toto) {
		
	}
	
	static function setNoCasServerValidation() {
		
	}

	static function isAuthenticated() {
		return true;
	}
	
}

class mockphpCAS2 {
	
	static function getUser() {
		return "tstark";
	}

	static function getAttributes() {
		return array(
			'prenom' => 'Tony',
			'nom' => 'Stark',
			'mail' => 'tony.stark@ac-versailles.fr',
			'description' => 'I am Iron Man',
			'FrEduRne' => Array('shield1$xx$yy$zz$shield', 'shield2$xx$yy$zz$shield')
		);
	}
	
	static function client($protocol, $hostname, $port, $baseuri, $toto) {
		
	}
	
	static function setNoCasServerValidation() {
		
	}
	
	static function isAuthenticated() {
		return true;
	}	
}


