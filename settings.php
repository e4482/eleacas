<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin settings and defaults.
 *
 * @package auth_cas
 * @copyright  2017 Stephen Bourget
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    require_once($CFG->dirroot.'/auth/eleacas/auth.php');
    require_once($CFG->dirroot.'/auth/eleacas/languages.php');

    $config = new config_eleacas();

    // Introductory explanation.
    $settings->add(new admin_setting_heading('auth_cas/pluginname', '',
        new lang_string('auth_casdescription', 'auth_cas')));

    // CAS server configuration label.
    $settings->add(new admin_setting_heading('auth_cas/casserversettings',
        new lang_string('auth_cas_server_settings', 'auth_cas'), ''));


    foreach ($config->get_websso() as $key => $value) {
        $checkedbydefault = isset($config->$key) ? $config->$key : false;
        $settings->add(new admin_setting_configcheckbox(
            'auth_eleacas/'. $key,
            $key,
            '',
            $checkedbydefault));
    }
}
