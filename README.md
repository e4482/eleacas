# Installation

* Just copy eleacas folder in the moodle/auth folder
* Using admin account, enable eleacas plugin in the admin moodle authentication section
* You can enable/disable access to CAS servers using eleacas parameters

# Params

CAS servers are hardcoded in the plugin. config.php is the file you must edit to define your own CAS servers.
This plugin is implemented to retrieve CAS attributes. It is designed to use attributes provided by CAS Apereo server.
At first user connection :

* if CAS server is configured to transmit user attributes, moodle will ask user to validate the account creation.
* if CAS server only transmit username attribute, moodle will notify customer to contact moodle admin to add current account manually.

Note : This plugin is using phpCAS scripts already available in the default moodle/auth/cas plugin

## Specific ELEA configuration (optional)

We store teacher RNE provided by Academic CAS Server in his profil thanks to a new entry 'rne1'. Following instructions insert such a new field in moodle
If you create this profil entry in your moodle, eleacas will fill it (if CAS server transmits user attributes)
You can customize 'rne1' name but be aware that you have to edit this name in the config.php file looking for 'profile_field_rne1'


```
  echo "INSERT INTO $dbname.mdl_user_info_category SET " \
	"id = '1', " \
	"name = 'Établissements', " \
	"sortorder = '1' " \
	";" | mysql --user=$dbuser --password=$dbpassword

  echo "INSERT INTO $dbname.mdl_user_info_field SET " \
	"shortname = 'rne1', " \
	"name = 'rne1', " \
	"datatype = 'text', " \
	"descriptionformat = '1', " \
	"categoryid = '1', " \
	"sortorder = '1', " \
	"required = '0', " \
	"locked = '0', " \
	"visible = '2', " \
	"forceunique = '0', " \
	"signup = '0', " \
	"param1 = '30', " \
	"param2 = '2048', " \
	"param3 = '0' " \
	";" | mysql --user=$dbuser --password=$dbpassword
```







