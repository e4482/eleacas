<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Authentication Plugin: CAS Authentication
 *
 * Authentication using CAS (Central Authentication Server).
 *
 * @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package auth_eleacas
 */
global $CFG;
include('auth/eleacas/auth.php');
class auth_plugin_eleacas_test extends advanced_testcase {

	function testConstructorIndex() {
		global $USER;
		$this->resetAfterTest(true);
		$ELEACAS = new auth_plugin_eleacas();
		$this->assertEquals("eleacas", $ELEACAS->authtype);
		$this->assertEquals("auth_eleacas", $ELEACAS->roleauth);
		$this->assertEquals("[AUTH CAS] ", $ELEACAS->errorlogtag);
		$this->assertEquals("auth_eleacas", $ELEACAS->pluginconfig);
		$this->assertEquals("array", gettype($ELEACAS->websso));
		
	}
	function testGetCASUsernameIndex() {

		$ELEACAS = new auth_plugin_eleacas('mockphpCAS');
		
		foreach($ELEACAS->websso as $key => $value) {
			$this->assertEquals($value['prefix'] . "." . "tstark", $ELEACAS->get_cas_username($key));
		}

	}
	function testGetUserInfoIndex() {

		$ELEACAS = new auth_plugin_eleacas('mockphpCAS');
		$_COOKIE['ELEA_authCAS'] = 'ACAD';
		$moodleattributes = Array();
		$moodleattributes = $ELEACAS->get_userinfo('tstark');
		$this->assertEquals("tstark", $moodleattributes['username']);
		$this->assertEquals("Tony", $moodleattributes['firstname']);
		$this->assertEquals("Stark", $moodleattributes['lastname']);
		$this->assertEquals("tony.stark@ac-versailles.fr", $moodleattributes['email']);
		$this->assertEquals("I am Iron Man", $moodleattributes['title']);
		$this->assertEquals("shield", $moodleattributes['profile_field_rne1']);
		
	}
	
	function testGetUserInfoMultipleEtabsIndex() {
		
		/*
		 * Check a multi-valued frEduRne field
		 */
		
		$ELEACAS = new auth_plugin_eleacas('mockphpCAS2');
		$_COOKIE['ELEA_authCAS'] = 'ACAD';
		$moodleattributes = Array();
		$moodleattributes = $ELEACAS->get_userinfo('tstark');
		$this->assertEquals("tstark", $moodleattributes['username']);
		$this->assertEquals("Tony", $moodleattributes['firstname']);
		$this->assertEquals("Stark", $moodleattributes['lastname']);
		$this->assertEquals("tony.stark@ac-versailles.fr", $moodleattributes['email']);
		$this->assertEquals("I am Iron Man", $moodleattributes['title']);
		$this->assertEquals("shield1", $moodleattributes['profile_field_rne1']);
		
	}
	
	function testUserLoginIndex() {
		$ELEACAS = new auth_plugin_eleacas('mockphpCAS');
		$this->resetAfterTest(true);


		// user from level5 connector does not exist on moodle
		// connection is forbidden and user is notified about account creation
		// on the fly
		$_COOKIE['ELEA_authCAS'] = 'ACAD';
		$_POST['authCAS'] = 'ACAD';		
		ob_start();
		$connected = $ELEACAS->user_login('acad.tstark', 'password_not_used');
		$out = ob_get_clean();
		$this->assertEquals(false, $connected);
		$this->assertContains("Your account will be created using following entries", $out);
		
		// user from level5 connector already exists on moodle
		// connection is allowed
		$user1 = $this->getDataGenerator()->create_user(array('email'=>'tony.stark@ac-versailles.fr', 'username'=>'acad.tstark'));
		$_COOKIE['ELEA_authCAS'] = 'ACAD';
		$_POST['authCAS'] = 'ACAD';		
		ob_start();
		$connected = $ELEACAS->user_login('acad.tstark', 'password_not_used');
		$out = ob_get_clean();
		$this->assertEquals(true, $connected);

		// user from level4 connector does not exist on moodle
		// connection is forbidden and user is notified that admin moodle must import account before
		$_COOKIE['ELEA_authCAS'] = 'ENT92';
		$_POST['authCAS'] = 'ENT92';		
		ob_start();
		$connected = $ELEACAS->user_login('ent92.tstark', 'password_not_used');
		$out = ob_get_clean();
		$this->assertEquals(false, $connected);
		$this->assertContains("Your admin does not created your account yet", $out);
		
	}	
	
	
	
	
	
	
}
