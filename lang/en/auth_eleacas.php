<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_eleacas', language 'en'.
 *
 * @package   auth_eleacas
 * @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['auth_eleadescription']='multi-cas plugin';
$string['pluginname'] = 'ELEA CAS server (SSO)';
$string['auth_eleacasdescription'] = '';
$string['enable_cas'] = 'Enable CAS ';
$string['account_not_created'] = "Your admin does not created your account yet";
$string['use_local_account'] = 'Utiliser mon compte local';
$string['account_creation'] = 'Your account will be created using following entries :';
$string['data_transfer'] = 'Transfering your data';
$string['account_missing'] = 'Account missing';
$string['CASform'] = 'ELEA CAS';
