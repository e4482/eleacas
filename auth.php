<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Authentication Plugin: CAS Authentication
 *
 * Authentication using CAS (Central Authentication Server).
 *
 * @author Martin Dougiamas
 * @author Jerome GUTIERREZ
 * @author Iñaki Arenaza
 * @author Pascal Fautrero <pascal.fautrero@ac-versailles.fr>
 * @author Rémi Lefeuvre <remi.lefeuvre@ac-versailles.fr>
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package auth_eleacas
 */

defined('MOODLE_INTERNAL') || die();

// use CAS.php from cas plugin

require_once($CFG->libdir . '/authlib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->dirroot . '/auth/cas/CAS/CAS.php');
require_once($CFG->dirroot . '/auth/eleacas/mockCAS.php');
require_once($CFG->dirroot . '/auth/eleacas/config.php');

/**
 * CAS authentication plugin.
 */
class auth_plugin_eleacas extends auth_plugin_base {

    /**
     * Constructor.
     */
    public function __construct($mock_phpcas = null) {
        $this->authtype = 'eleacas';
        $this->roleauth = 'auth_eleacas';
        $this->errorlogtag = '[AUTH CAS] ';
        $this->pluginconfig = 'auth_' . $this->authtype;
        $this->config = get_config($this->pluginconfig);
        $config_eleacas = new config_eleacas();
        $this->websso = $config_eleacas->get_websso();
        $this->mock = $mock_phpcas;

        if ($mock_phpcas !== null) {
            $this->phpCAS = new $mock_phpcas;
        } else {
            $this->phpCAS = new phpCAS;
        }
    }

    /**
     * 
     * Add prefix to default username
     * @return string prefix.username
     * 
     */
    function get_cas_username($authCAS) {
        $phpCAS = $this->phpCAS;
        $cas_username = $this->websso[$authCAS]['prefix'] .
                "." .
                trim(core_text::strtolower($phpCAS::getUser()));
        return $cas_username;
    }

    /**
     * 
     * 
     * 
     * 
     */
    function prevent_local_passwords() {
        return true;
    }

    /**
     * Authenticates user against CAS
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username (with system magic quotes)
     * @param string $password The password (with system magic quotes)
     * @return bool Authentication success or failure.
     */
    function user_login($username, $password) {
        global $DB, $PAGE, $OUTPUT, $CFG;
        $authCAS = optional_param('authCAS', '', PARAM_RAW);

        if (!array_key_exists($authCAS, $this->websso)) {
            $user = $DB->get_record('user', array('username' => $username));

            if (!is_object($user) || !$user->firstname || !$user->lastname || !$user->email) {
                $PAGE->set_url('/login/index.php');
                $PAGE->set_title(get_string('account_missing', 'auth_eleacas'));
                $PAGE->set_heading("HEADING");
                echo $OUTPUT->header();
                include($CFG->dirroot . '/auth/eleacas/account_error.html');
                echo $OUTPUT->footer();
                if (CLI_SCRIPT or AJAX_SCRIPT) {
                    return false;
                }
                exit();
            }
        }
        $this->connectCAS($authCAS);
        $phpCAS = $this->phpCAS;
        if ($phpCAS::isAuthenticated() && ($this->get_cas_username($authCAS) == $username)) {

            if (array_key_exists($authCAS, $this->websso) && $this->websso[$authCAS]['level'] == 5) {
                $user = $DB->get_record('user', array('username' => $username));
                $validation = optional_param('validation', '', PARAM_RAW);
                if (!is_object($user) && !$validation) {
                    $data = array();
                    $cas_config = new config_eleacas;
                    foreach ($phpCAS::getAttributes() as $key => $value) {
                        if (array_key_exists($key, $this->websso[$authCAS]['lut'])) {
                            $key_lut = $this->websso[$authCAS]['lut'][$key];
                            if (is_array($value))
                                $value = $value[0];
                            $value = $cas_config->apply_filter($key, $value);
                            $data[$key_lut] = array("value" => $value, "label" => $this->websso[$authCAS]['label'][$key]);
                        }
                    }

                    $PAGE->set_url('/login/index.php');
                    $PAGE->set_title(get_string('data_transfer', 'auth_eleacas'));
                    $PAGE->set_heading("HEADING");
                    echo $OUTPUT->header();
                    include($CFG->dirroot . '/auth/eleacas/form_validation.html');
                    echo $OUTPUT->footer();
                    if (CLI_SCRIPT or AJAX_SCRIPT) {
                        return false;
                    }
                    exit();
                }
            } else {
                $user = $DB->get_record('user', array('username' => $username));

                if (!is_object($user) || !$user->firstname || !$user->lastname || !$user->email) {
                    // available for unit tests
                    $PAGE->set_url('/login/index.php');
                    $PAGE->set_title(get_string('account_missing', 'auth_eleacas'));
                    $PAGE->set_heading("HEADING");
                    echo $OUTPUT->header();
                    include($CFG->dirroot . '/auth/eleacas/account_error.html');
                    echo $OUTPUT->footer();
                    if (CLI_SCRIPT or AJAX_SCRIPT) {
                        return false;
                    }
                    exit();
                }
            }
        }
        return $phpCAS::isAuthenticated() && ($this->get_cas_username($authCAS) == $username);
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return false;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return false;
    }

    /**
     * Authentication choice (CAS or other)
     * Redirection to the CAS form or to login/index.php
     * for other authentication
     */
    function loginpage_hook() {
        global $frm;
        global $CFG;
        global $SESSION, $OUTPUT, $PAGE;

        $site = get_site();
        $CASform = get_string('CASform', 'auth_eleacas');
        $username = optional_param('username', '', PARAM_RAW);
        $courseid = optional_param('courseid', 0, PARAM_INT);
        $phpCAS = $this->phpCAS;

        if (!empty($username)) {
            if (isset($SESSION->wantsurl) && (strstr($SESSION->wantsurl, 'ticket') ||
                    strstr($SESSION->wantsurl, 'LOCAL'))) {
                unset($SESSION->wantsurl);
            }
            return;
        }


        // If there is an authentication error, stay on the default authentication page.
        if (!empty($SESSION->loginerrormsg)) {
            return;
        }

        $authCAS = optional_param('authCAS', '', PARAM_RAW);
        if ($authCAS == 'LOCAL') {
            return;
        }

        // Show authentication form for multi-authentication.
        // Test pgtIou parameter for proxy mode (https connection in background from CAS server to the php server).
        if (!array_key_exists($authCAS, $this->websso) && !isset($_GET['pgtIou'])) {

            $PAGE->set_url('/login/index.php');
            $PAGE->navbar->add($CASform);
            $PAGE->set_title("$site->fullname: $CASform");
            $PAGE->set_heading($site->fullname);
            echo $OUTPUT->header();
            include($CFG->dirroot . '/auth/eleacas/cas_form.html');
            echo $OUTPUT->footer();
            if (CLI_SCRIPT or AJAX_SCRIPT) {
                return false;
            }
            exit();
        }

        // Connection to CAS server
        $this->connectCAS($authCAS);
        //phpCAS::setDebug("/tmp/phpCAS.log");
        if ($phpCAS::checkAuthentication()) {
            $frm = new stdClass();
            $frm->username = $this->get_cas_username($authCAS);
            $frm->password = 'passwdCas';
            $frm->logintoken = \core\session\manager::get_login_token();

            // Redirect to a course if multi-auth is activated, 
            // authCAS is set to CAS and the courseid is specified.
            if (true && !empty($courseid)) {
                redirect(new moodle_url('/course/view.php', array('id' => $courseid)));
            }
            return;
        }

        if (isset($_GET['loginguest']) && ($_GET['loginguest'] == true)) {
            $frm = new stdClass();
            $frm->username = 'guest';
            $frm->password = 'guest';
            $frm->logintoken = \core\session\manager::get_login_token();
            return;
        }

        // Force CAS authentication (if needed).
        if (!$phpCAS::isAuthenticated()) {
            //phpCAS::setLang($this->config->language);
            $phpCAS::forceAuthentication();
        }
    }

    /**
     * Connect to the CAS (clientcas connection or proxycas connection)
     *
     */
    function connectCAS($authCAS) {
        global $CFG, $SESSION;
        static $connected = false;
        $phpCAS = $this->phpCAS;
        if (!$connected || ($authCAS != $_COOKIE['ELEA_authCAS'])) {

            $hostname = "";
            $port = "";
            $baseuri = "";
            $protocol = "";
            if (array_key_exists($authCAS, $this->websso)) {
                $hostname = $this->websso[$authCAS]["hostname"];
                $protocol = $this->websso[$authCAS]["protocol"];
                $port = $this->websso[$authCAS]["port"];
                $baseuri = $this->websso[$authCAS]["baseuri"];
            }

            // naughty hack : destroy phpCAS session infos
            // if trying to connect with new CAS server.

            if (array_key_exists('ELEA_authCAS', $_COOKIE)) {
                if ($authCAS != $_COOKIE['ELEA_authCAS']) {
                    unset($_SESSION['phpCAS']);
                }
            }
            // naughty hack to remember authCAS at logout...

            @setcookie("ELEA_authCAS", $authCAS);
            $phpCAS::client($protocol, $hostname, (int) $port, $baseuri, false);

            $connected = true;
        }

        /* Warning : Hack to pass through apache http auth on pprod Lilie CAS */
        //phpCAS::setExtraCurlOption(CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //phpCAS::setExtraCurlOption(CURLOPT_USERPWD, 'preprod:lilieenautomne');
        // If Moodle is configured to use a proxy, phpCAS needs some curl options set.
        if (!empty($CFG->proxyhost) && !is_proxybypass($phpCAS::getServerLoginURL())) {
            $phpCAS::setExtraCurlOption(CURLOPT_PROXY, $CFG->proxyhost);
            if (!empty($CFG->proxyport)) {
                $phpCAS::setExtraCurlOption(CURLOPT_PROXYPORT, $CFG->proxyport);
            }
            if (!empty($CFG->proxytype)) {
                // Only set CURLOPT_PROXYTYPE if it's something other than the curl-default http
                if ($CFG->proxytype == 'SOCKS5') {
                    $phpCAS::setExtraCurlOption(CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
                }
            }
            if (!empty($CFG->proxyuser) and ! empty($CFG->proxypassword)) {
                $phpCAS::setExtraCurlOption(CURLOPT_PROXYUSERPWD, $CFG->proxyuser . ':' . $CFG->proxypassword);
                if (defined('CURLOPT_PROXYAUTH')) {
                    // any proxy authentication if PHP 5.1
                    $phpCAS::setExtraCurlOption(CURLOPT_PROXYAUTH, CURLAUTH_BASIC | CURLAUTH_NTLM);
                }
            }
        }
        $phpCAS::setNoCasServerValidation();
    }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        global $CFG, $OUTPUT;
        include($CFG->dirroot . '/auth/eleacas/config.html');
    }

    /**
     * A chance to validate form data, and last chance to
     * do stuff before it is inserted in config_plugin
     * @param object object with submitted configuration settings (without system magic quotes)
     * @param array $err array of error messages
     */
    function validate_form($form, &$err) {
        
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null;
    }

    /**
     *
     * @param string $username username
     * @return mixed array with no magic quotes or false on error
     */
    function get_userinfo($username) {
        $moodleattributes = array();
        $moodleattributes['username'] = core_text::strtolower(trim($username));
        $phpCAS = $this->phpCAS;
        $key_connector = $_COOKIE['ELEA_authCAS'];
        if ($key_connector === 'ACAD') {
            $moodleattributes['profile_field_profil'] = 'teacher';
        }
        if (array_key_exists($key_connector, $this->websso) && $this->websso[$key_connector]['level'] == 5) {
            $cas_config = new config_eleacas;
            foreach ($phpCAS::getAttributes() as $key => $value) {
                // Look up table to convert CAS entries to moodle entries

                if (array_key_exists($key, $this->websso[$key_connector]['lut'])) {
                    if (is_array($value))
                        $value = $value[0];
                    $value = $cas_config->apply_filter($key, $value);

                    if ($key === "FrEduRne") {
                        $value = strtolower($value);
                    }

                    $key_lut = (array) $this->websso[$key_connector]['lut'][$key];
                    foreach ($key_lut as $kl) {
                        if (is_array($value)) {
                            $moodleattributes[$kl] = $value[0];
                        } else {
                            $moodleattributes[$kl] = $value;
                        }
                    }
                }
            }
        }
        return $moodleattributes;
    }

    /**
     * Syncronizes users from LDAP server to moodle user table.
     *
     * If no LDAP servers are configured, simply return. Otherwise,
     * call parent class method to do the work.
     *
     * @param bool $do_updates will do pull in data updates from LDAP if relevant
     * @return nothing
     */
    function sync_users($do_updates = true) {
        echo "nothing to do...";
        return;
    }

    /**
     * Hook for logout page
     */
    function logoutpage_hook() {
        global $USER, $redirect;

        // Only do this if the user is actually logged in via CAS
        if ($USER->auth === $this->authtype) {
            // Check if there is an alternative logout return url defined
            //$redirect = $this->config->logout_return_url;
        }
    }

    /**
     * Post logout hook.
     *
     * Note: this method replace the prelogout_hook method to avoid redirect to CAS logout
     * before the event userlogout being triggered.
     *
     * @param stdClass $user clone of USER object object before the user session was terminated
     */
    public function postlogout_hook($user) {
        global $CFG;
        $phpCAS = $this->phpCAS;
        // Only redirect to CAS logout if the CAS server is compatible and the user is logged as a CAS user
        if ($user->auth == $this->authtype && $this->websso[$_COOKIE['ELEA_authCAS']]['logout']) {
            $backurl = $CFG->wwwroot;
            $this->connectCAS($_COOKIE['ELEA_authCAS']);
            $phpCAS::logoutWithRedirectService($backurl);
        }
    }

}
