<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Authentication Plugin: CAS Authentication
 *
 * @author Pascal Fautrero
 * @author Rémi Lefeuvre
 * @package auth_eleacas
 */

defined('MOODLE_INTERNAL') || die();

/**
 * WebSSO config
 */
class config_eleacas {
    /*
     * 	Array used to define CAS servers
     *
     *  title       : button title used in the "WAYF" page cas_form.html
     *  hostname    : CAS server hostname
     *  protocol    : CAS protocol used
     *  port        : CAS port
     *  baseuri     : CAS server suffix used
     *  logout      : true if logout is possible against CAS server
     *  prefix      : used in moodle to prefix username, some kind of namespace
     *  level       : if '5', attributes are given by CAS server
     *  lut         : look up table to map moodle attributes and CAS attributes (used if level=5)
     *  label       : look up table to get labels for form_validation.html (used if level=5)
     */

    private $websso = [
        "DANE" => array(
            "title"     => "Utiliser mon compte DANE",
            "hostname"  => "websso.crdp.ac-versailles.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => true,
            "prefix"    => "dane",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "ACAD" => array(
            "title"     => "Utiliser mon compte académique",
            "hostname"  => "edu-cas.ac-versailles.fr",
            "protocol"  => SAML_VERSION_1_1,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => true,
            "prefix"    => "acad",
            "level"     => 5,
            "lut"       => array(
                "prenom"        => "firstname",
                "nom"           => "lastname",
                "mail"          => "email",
                "description"   => "title",
                "FrEduRne"      => ["profile_field_rne1", "profile_field_uai"]
            ),
            "label"     => array(
                "prenom"        => "Prénom",
                "nom"           => "Nom",
                "mail"          => "Adresse mail",
                "description"   => "Titre",
                "FrEduRne"      => "RNE"
            )
        ),
        "LILIE" => array(
            "title"     => "Utiliser mon compte monlycée.net",
            "hostname"  => "ent.iledefrance.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => false,
            "prefix"    => "entidf",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "ENT92" => array(
            "title"     => "Utiliser mon compte ENC 92",
            "hostname"  => "www.enc92.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => true,
            "prefix"    => "ent92",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "ENT78" => array(
            "title"     => "Utiliser mon compte eCollège",
            "hostname"  => "ecollege.yvelines.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => true,
            "prefix"    => "ent78",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "ELANCOURT" => array(
            "title"     => "Utiliser mon compte Elancourt",
            "hostname"  => "ent.enteduc.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => true,
            "prefix"    => "elancourt",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "ENT91" => array(
            "title"     => "Utiliser mon compte MonCollege.essonne.fr",
            "hostname"  => "www.moncollege-ent.essonne.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => false,
            "prefix"    => "ent91",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "OZE78" => array(
            "title"     => "Utiliser OZE",
            "hostname"  => "ozecollege.yvelines.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => false,
            "prefix"    => "oze78",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "OZE781D" => array(
            "title"     => "Utiliser OZE 1er dégré",
            "hostname"  => "ecole.yvelinesnum.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => false,
            "prefix"    => "oze781d",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "OZE92" => array(
            "title"     => "Utiliser OZE",
            "hostname"  => "enc.hauts-de-seine.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => false,
            "prefix"    => "oze92",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "OZETEST" => array(
            "title"     => "OZE TEST",
            "hostname"  => "test.oze.education",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => false,
            "prefix"    => "ozetest",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "OZEDEV" => array(
            "title"     => "OZE DEV",
            "hostname"  => "dev.oze.education",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "cas/",
            "logout"    => false,
            "prefix"    => "ozedev",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "KOS95" => array(
            "title"     => "Utiliser mon compte moncollege.valdoise.fr",
            "hostname"  => "cas.moncollege.valdoise.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "",
            "logout"    => false,
            "prefix"    => "kos95",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        ),
        "KOSTEST" => array(
            "title"     => "KOSMOS TEST",
            "hostname"  => "cas.sandbox.kdecole.fr",
            "protocol"  => CAS_VERSION_2_0,
            "port"      => 443,
            "baseuri"   => "",
            "logout"    => false,
            "prefix"    => "kos95",
            "level"     => 4,
            "lut"       => NULL,
            "label"     => NULL
        )
    ];

    function __construct() {

    }

    /**
     * Get CAS list servers
     * @return Array
     *
     */
    public function get_websso() {
        return $this->websso;
    }

    /**
     * Apply filter on specific entry
     *
     * FrEduRne CAS attribute is a stringify Array using '$' as separator
     * FrEduRne = UAA$xx$yy$zz$UAJ
     *   UAA = Unite Académique d'Affectation
     *   UAJ = Unité Académique Juridique
     * First entry of this array is the user RNE (UAA)
     * This filter will extract this entry
     * @key string CAS key attribute
     * @value string CAS value attribute
     * @return string filtered $value
     *
     */
    public function apply_filter($key, $value) {
        // specific filter for ACAD connector
        if ($key == 'FrEduRne') {
            $array_value = preg_split('/\$/', $value, '-1');
            $value = $array_value[0];
        }
        return $value;
    }

}
